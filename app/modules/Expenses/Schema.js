const mongoose = require('mongoose');
var schema = mongoose.Schema;

const expenseSchema = new mongoose.Schema({
    amount: { type: Number, required: true },
    userId: { type: schema.Types.ObjectId, ref: 'Users' },
    categoryId: { type: schema.Types.ObjectId, ref: 'Category' },
    categoryName: { type: String },
    status: { type: String, default: 'Active' },
    isDeleted: { type: Boolean, default: false },
}, {
    timestamps: true
});

const Expenses = mongoose.model('expenses', expenseSchema);
module.exports = {
    Expenses
}