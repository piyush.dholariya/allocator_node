module.exports = (app, express) => {
    const router = express.Router();
    const config = require('../../../configs/configs');
    const Globals = require("../../../configs/Globals");
    const ExpenseController = require('./Controller');

    router.post('/expense/addExpense', (req, res, next) => {
        const expenseObj = (new ExpenseController()).boot(req, res);
        return expenseObj.addExpense();
    });

    router.get('/expense/getExpense', Globals.isAuthorised, (req, res, next) => {
        const expenseObj = (new ExpenseController()).boot(req, res);
        return expenseObj.getExpense();
    });

    router.post('/expense/deleteExpense', Globals.isAuthorised, (req, res, next) => {
        const expenseObj = (new ExpenseController()).boot(req, res);
        return expenseObj.deleteExpense();
    });

    app.use(config.baseApiUrl, router);
}