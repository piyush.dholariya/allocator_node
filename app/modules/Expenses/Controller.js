const _ = require('lodash');
const Controller = require('../Base/Controller');
const Expenses = require('./Schema').Expenses;
const Category = require('../Category/Schema').Category;
const RequestBody = require("../../services/RequestBody");
const ObjectId = require('mongodb').ObjectId;
const Model = require("../Base/Model");
const i18n = require("i18n");

class ExpenseController extends Controller {
    constructor() {
        super();
    }


    async addExpense() {
        try {
            if (this.req.body._id) {
                let fieldsArray = ["amount"];
                let data = await (new RequestBody()).processRequestBody(this.req.body, fieldsArray);
                let expenseData = await Expenses.findByIdAndUpdate(currentUser, { amount: data.amount }).select({ categoryName: 1, amount: 1 });
                if (expenseData) return this.res.send({ status: 1, data: expenseData });
                return this.res.send({ status: 0, message: i18n.__("EXPENSE_NOT_UPDATED") });
            }

            let fieldsArray = ["amount", "userId", "categoryId"];
            let data = await (new RequestBody()).processRequestBody(this.req.body, fieldsArray);
            let expense = await Expenses.findOne({ userId: ObjectId(data.userId), amount: data.amount, categoryId: ObjectId(data.categoryId) });
            if (!_.isEmpty(expense)) return this.res.send({ status: 0, message: i18n.__("EXPENSE_ALREADY_ADDED") });

            let categoryData = await Category.findById({ _id: ObjectId(data.categoryId) }).select({ categoryName: 1 }).lean();
            // console.log("----categoryData----", categoryData.categoryName);
            data.categoryName = categoryData.categoryName;

            const newUserId = await new Model(Expenses).store(data);
            return this.res.send({ status: 1, message: i18n.__("EXPENSE_ADDED_SUCCESSFULLY") });
        } catch (error) {
            console.log("----error------", error);
            // this.res.send({ status: 0, message: error });
            return this.res.send({ status: 0, message: i18n.__("SERVER_ERROR") });
        }
    }

    async getExpense() {
        try {
            let currentUser = this.req.currentUser && this.req.currentUser._id ? this.req.currentUser._id : "";
            let expenseData = await Expenses.find({ userId: currentUser, isDeleted: false }).select({ categoryName: 1, amount: 1 }).lean();
            return this.res.send({ status: 1, data: expenseData });
        } catch (error) {
            console.log("----error------", error);
            return this.res.send({ status: 0, message: i18n.__("SERVER_ERROR") });
        }
    }

    async deleteExpense() {
        try {
            let currentUser = this.req.currentUser && this.req.currentUser._id ? this.req.currentUser._id : "";
            let expenseData = await Expenses.findByIdAndUpdate(currentUser, { isDeleted: true });
            if (expenseData) return this.res.send({ status: 1, message: i18n.__("EXPENSE_DELETED_SUCCESSFULLY") });
            return this.res.send({ status: 0, message: i18n.__("EXPENSE_NOT_DELETED") });
        } catch (error) {
            console.log("----error------", error);
            return this.res.send({ status: 0, message: i18n.__("SERVER_ERROR") });
        }
    }

}

module.exports = ExpenseController;