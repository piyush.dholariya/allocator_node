const mongoose = require('mongoose');
var schema = mongoose.Schema;

const categorySchema = new mongoose.Schema({
    categoryName: { type: String },
    addedBy: { type: schema.Types.ObjectId, ref: 'Users' },
    status: { type: String, default: 'Active' },
    isDeleted: { type: Boolean, default: false },
}, {
    timestamps: true
});

const Category = mongoose.model('categories', categorySchema);
module.exports = {
    Category
}