/****************************
 Configuration
 ****************************/
module.exports = {
    // db: 'mongo "mongodb+srv://cluster0-4vszs.mongodb.net/test"  --username allocator',
    // db: 'mongodb+srv://allocator:allocator123@cluster0-4vszs.mongodb.net/test',
    // db: 'mongodb+srv://allocator:allocator123@cluster0-4vszs.mongodb.net/allocator_db?retryWrites=true&w=majority',
    db: 'mongodb://localhost:27017/allocator',
    mongoDBOptions: {
        reconnectTries: Number.MAX_VALUE,
        reconnectInterval: 1000,
        keepAlive: 1,
        connectTimeoutMS: 30000,
        useNewUrlParser: true,
        useFindAndModify: false,
        native_parser: true,
        poolSize: 5,
        // user: 'allocator',
        // pass: 'allocator'
    },

    sessionSecret: 'indNIC2305',
    securityToken: 'indNIC2305',
    securityRefreshToken: 'indNIC2305refresh',

    baseApiUrl: '/api',
    host: "10.2.3.124",
    serverPort: '5000',
    tokenExpiry: 361440, // Note: in seconds! (1 day)

    rootUrl: 'http://localhost/api',
    frontUrl: 'http://10.2.99.23:4003',
    frontUrlAngular: 'http://ng7adminseed.node.indianic.com/#/public',

    defaultEmailId: 'meanstack2017@gmail.com',
    apiUrl: 'http://10.2.3.124',

    perPage: 20,
    adPerPage: 4,

    s3upload: false,
    localImagePath: "/public/upload/images/",
    s3ImagePath: "",

    dontAllowPreviouslyUsedPassword: true,
    storePreviouslyUsedPasswords: true,

    forceToUpdatePassword: true,
    updatePasswordPeriod: 4, // In months

    allowedFailAttemptsOfLogin: 5,
    isBlockAfterFailedAttempt: true,
    timeDurationOfBlockingAfterWrongAttempts: 15, // In minutes

    tokenExpirationTime: 540, // minutes
    forgotTokenExpireTime: 60, // minutes
    verificationTokenExpireTime: 60, // minutes

    extendTokenTime: true,
    useRefreshToken: true,

    isHTTPAuthForSwagger: false,
    HTTPAuthUser: "root",
    HTTPAuthPassword: "root"
};
