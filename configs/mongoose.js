/****************************
 MONGOOSE SCHEMAS
 ****************************/
let config = require('./configs');
let mongoose = require('mongoose');
const MongoClient = require('mongodb').MongoClient;

mongoose.Promise = global.Promise;

module.exports = async () => {


    // const client = new MongoClient(config.db, config.mongoDBOptions);
    // await client.connect(async err => {
    //     console.log('MongoDB connected');
    //     // await listDatabases(client);
    //     // client.close();
    // })


    var db = mongoose.connect(config.db).then(
        (connect) => { console.log('MongoDB connected') },
        (err) => { console.log('MongoDB connection error', err) }
    );
    mongoose.set('useCreateIndex', true);
    // return client;
    return db;
};
